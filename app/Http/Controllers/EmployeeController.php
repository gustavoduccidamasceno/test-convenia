<?php

namespace App\Http\Controllers;

use App\Imports\EmployeesImport;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{

    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function import(Request $request): JsonResponse
    {
        Excel::import(new EmployeesImport, $request->file('file'));

        Mail::raw('Employees created/updated successfully.', function ($message) use ($request) {
            $message->to($request->user()->email)->subject('Changes in employees');
        });

        return response()->json([
            'success' => true,
            'message' => 'Excel imported successfully',
        ], Response::HTTP_OK);
    }
}
