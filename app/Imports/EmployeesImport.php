<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Row;

class EmployeesImport implements OnEachRow, WithHeadingRow, WithValidation
{
    use Importable;

    public function onRow(Row $row): void
    {
        $row = $row->toArray();

        Employee::updateOrCreate([
            'document' => $row['document'],
        ], [
            'user_id' => auth()->user()->id,
            'email' => $row['email'],
            'name' => $row['name'],
            'city' => $row['city'],
            'state' => $row['state'],
            'start_date' => $row['start_date'],
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'document' => 'required|int',
            'city' => 'required|string',
            'state' => 'required|string',
            'start_date' => 'required|date|date_format:Y-m-d',
        ];
    }
}
